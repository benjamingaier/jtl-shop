# HOOK_CHECKBOX_CLASS_GETCHECKBOXFRONTEND (180)

## Triggerpunkt

Nach dem Holen aller Checkboxes

## Parameter

* `Collection` **&oCheckBox_arr** - Liste der Checkboxes
* `int` **nAnzeigeOrt** - Anzeigeort
* `int` **kKundengruppe** - Kundengruppen-ID
* `bool` **bAktiv** - Flag, ob aktiv oder nicht
* `bool` **bSprache** - Sprach-ID
* `bool` **bSpecial** - Flag für "spezielle Checkbox?"
* `bool` **bLogging** - Logging aktiv oder nicht