# HOOK_NAVI_PRESUCHE (160)

## Triggerpunkt

Bevor mit der Suche begonnen wird

## Parameter

* `stdClass` **&cValue** - Suchbegriff
* `bool` **&bExtendedJTLSearch** - Flag, welches die erweitere Suche anzeigt