# HOOK_PRODUCTFILTER_GET_BASE_QUERY (253)

## Triggerpunkt

Beim Erzeugen des Artikelfilter-Basisabfrage-Objektes

## Parameter

* `array` **&select** - für die Abfrage verwendete SELECTs
* `array` **&joins** - für die Abfrage verwendete JOINs
* `array` **&conditions** - für die Abfrage verwendete WHEREs
* `array` **&groupBy** - für die Abfrage verwendete GROUP BYs
* `array` **&having** - für die Abfrage verwendete HAVINGs
* `array` **&order** - für die Abfrage verwendete ORDERs
* `array` **&limit** - für die Abfrage verwendete LIMITs
* `JTL\Filter\ProductFilter` **productFilter** - aktuelles Artikelfilter-Objekt