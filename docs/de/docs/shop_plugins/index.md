# Plugins

* [Allgemein](allgemein.md)  
  Allgemeine Informationen über das Plugin-System

* [Aufbau](aufbau.md)  
  Ordner- und Dateistruktur

* [Die info.xml](infoxml.md)  
  Aufbau der `info.xml`

* [Bootstrapping](bootstrapping.md)  
  Der Bootstrapping-Prozess im JTL-Shop 5.x

* [Variablen](variablen.md)  
  Verfügbare Plugin-Variablen

* [Hooks](hooks.md)  
  Allgemeines über Hooks im JTL-Shop

* [Hook-Liste](hook_list.md)  
  Liste alles Hooks des JTL-Shops

* [Container](container.md)  
  Der "Dependency Injection Container" (ab JTL-Shop 5.x)

* [Cache](cache.md)  
  Umgang mit dem Objektcache im JTL-Shop

* [Cron-Jobs](cron_jobs.md)  
  Registrieren eigener Cron-Jobs

* [Mails](mailing.md)  
  Versenden von Emails

* [Portlets](portlets.md)  
  Den OnPageComposer erweitern

* [Routen](routes.md)  
  Eigene Routen definieren

* [Logging](logging.md)
  Logging

* [Fehlercodes](fehlercodes.md)  
  Mögliche Fehlercodes bei der Plugininstallation

* [Sichere Plugins schreiben](sicherheit.md)  
  Programmierung von sicheren Plugins im JTL-Shop

* [Zahlungs-Plugins](payment_plugins.md)  
  Oder: "Wie man ein Payment-Plugin schreibt"

* [Hinweise, Tipps & Tricks](hinweise.md)  
  Allgemeine Hinweise, Tipps und Tricks zur Entwicklung von Plugins
