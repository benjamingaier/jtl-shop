# Notifications

Im Backend von JTL-Shop 5 gibt es die Möglichkeit, wichtige Informationen für den Shop-Administrator an zentraler
Stelle auszugeben.

![](_images/backend_notification-area.png)

Dieser Mechanismus wird "Notifications" genannt und steht ausschließlich im Backend des Onlineshops zur Verfügung.

Notifications werden vorwiegend dazu eingesetzt, auf Probleme oder Fehler in den Konfigurationen aufmerksam zu machen.
Erzeugt werden können Notifications ebenso von einem Plugin. Nutzen Sie dazu die Methode `add` aus dem Singleton
`\JTL\Backend\Notification`:

    public function add(int $type, string $title, string $description = null, string $url = null, string $hash = null)

## Beispiel

    Notification::getInstance()
        ->add(
           NotificationEntry::TYPE_WARNING,
           $this->getPlugin()->getMeta()->getName(),
           'Plugin nicht konfiguriert!',
           Shop::getAdminURL() . '/plugin.php?kPlugin=' . $this->getID()
        );

Parameter der Methode `add()`

| Parameter | Verwendung |
| - | - |
| `$type` | Priorität der Notification (siehe: [NotificationEntry Typen](#notificationentry-typen)) |
| `$title` | Titeltext |
| `$description` | (optional) Beschreibungstext |
| `$url` | (optional) Linkziel, wenn die Notification auf eine Backendseite weiterleiten soll |
| `$hash` | (optional) Identifikator-String. Falls gesetzt ist die Notification abweisbar (siehe: [Abweisbare Notifications](#abweisbare-notifications) |

.. _label_notifications_type:

## NotificationEntry Typen

| Konstante | Wert | mögliche Verwendung |
| - | - | - |
| `TYPE_INFO`    | `0` | (Farbe: hellgrau) allgemeine Informationen |
| `TYPE_WARNING` | `1` | (Farbe: orange) Warnungen zu Einstellungen, die den ordnungsgemäßen Betrieb des Onlineshops beeinträchtigen können |
| `TYPE_DANGER`  | `2` | (Farbe: rot) Warnungen zu kritischen Einstellungen und Fehlern |

Das Ausgeben aller Notifications erfolgt bei der Initialisierung des Shop-Backends. An dieser Stelle wird auch das
Dispatcher-Event `backend.notification` ausgelöst. Über dieses Event ist es Plugins möglich, eigene Notifications
zu erzeugen.

!!! attention
    Die Erzeugung eines NotificationEntrys sollte keine zeitkritischen Programmschritte enthalten,
    da diese das Onlineshop-Backend blockieren können.

# Abweisbare Notifications

Notifications können als "abweisbar" konfiguriert werden, so dass der Shop-Administrator die Notification mit einem
Klick auf das Kreuz als gelesen ausblenden kann.

Dazu muss der optionale Parameter `$hash` der `add`-Methode einen eindeutigen Bezeichner enthalten. Beispiel:

    $this->add(
        NotificationEntry::TYPE_WARNING,
        \__('validModifiedFileStructTitle'),
        \__('validModifiedFileStructMessage'),
        $adminURL . Route::FILECHECK,
        'validModifiedFileStruct'
    );