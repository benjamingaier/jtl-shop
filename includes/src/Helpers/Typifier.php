<?php declare(strict_types=1);

namespace JTL\Helpers;

/**
 * Class Attribute
 * @since 5.3.0
 * @package JTL\Helpers
 */
class Typifier
{
    /**
     * @var array
     */
    private const POSSIBLEBOOLVALUES = [
        'true'  => true,
        'y'     => true,
        'yes'   => true,
        'ja'    => true,
        '1'     => true,
        'false' => false,
        'n'     => false,
        'no'    => false,
        'nein'  => false,
        '0'     => false,
    ];

    private const TYPEMAPPING = [
        'string'  => 'stringify',
        'integer' => 'intify',
        'double'  => 'floatify',
        'boolean' => 'boolify',
        'array'   => 'arrify',
        'object'  => 'objectify',
    ];

    /**
     * @param mixed  $value
     * @param string $to
     * @return mixed
     */
    public static function typeify(mixed $value, string $to): mixed
    {
        $method = self::TYPEMAPPING[$to] ?? '';
        if ($method === '') {
            return $value;
        }

        return self::$method($value);
    }

    /**
     * @param mixed $value
     * @param string|null $default
     * @return string|null
     */
    public static function stringify(mixed $value, ?string $default = ''): ?string
    {
        if ($value === null) {
            return $default;
        }

        if (\is_string($value)) {
            return $value;
        }

        throw new \RuntimeException('Value could not be typified into a string.');
    }

    /**
     * @param mixed  $value
     * @param string $separator
     * @param bool   $addLeadingAndTrailingSeparator
     * @return string
     */
    public static function stringFromArray(
        mixed $value,
        string $separator = ';',
        bool $addLeadingAndTrailingSeparator = false
    ): string {
        if (\is_array($value)) {
            if ($addLeadingAndTrailingSeparator === true) {
                $result = $separator . \implode($separator, $value) . $separator;
            } else {
                $result = \implode($separator, $value);
            }
        } else {
            $result = (\is_string($value) ? $value : '');
        }

        return $result;
    }

    /**
     * @param mixed $value
     * @param int|null $default
     * @return int|null
     */
    public static function intify(mixed $value, ?int $default = 0): ?int
    {
        if ($value === null) {
            return $default;
        }

        if (\is_numeric($value)) {
            return (int)$value;
        }

        throw new \RuntimeException('Value could not be typified into a integer.');
    }

    /**
     * @param $value
     * @param float|null $default
     * @return float|null
     */
    public static function floatify($value, ?float $default = 0.00): ?float
    {
        if ($value === null) {
            return $default;
        }

        if (\is_numeric($value)) {
            return (float)$value;
        }

        throw new \RuntimeException('Value could not be typified into a float.');
    }

    /**
     * @param $value
     * @param bool|null $default
     * @return bool|null
     */
    public static function boolify($value, ?bool $default = false): ?bool
    {
        if ($value === null) {
            return $default;
        }

        if (\is_bool($value)) {
            return $value;
        }

        $value = \strtolower((string)$value);
        if (!\array_key_exists($value, self::POSSIBLEBOOLVALUES)) {
            throw new \RuntimeException('Value is not a boolean and could not be typified into one');
        }

        return self::POSSIBLEBOOLVALUES[$value];
    }

    /**
     * @param $value
     * @param array|null $default
     * @return array|null
     */
    public static function arrify($value, ?array $default = []): ?array
    {
        if ($value === null) {
            return $default;
        }

        if (\is_array($value)) {
            return $value;
        }

        throw new \RuntimeException('Value could not be typified into a array.');
    }

    /**
     * @param $value
     * @param object|null $default
     * @return object|null
     */
    public static function objectify($value, ?object $default = new \stdClass()): ?object
    {
        if ($value === null) {
            return $default;
        }

        if (\is_object($value)) {
            if (\method_exists($value, 'toObject')) {
                return $value->toObject();
            }

            return $value;
        }

        throw new \RuntimeException('Value could not be typified into a object.');
    }
}
