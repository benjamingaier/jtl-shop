<?php declare(strict_types=1);

namespace JTL\RMA\Repositories;

use JTL\Abstracts\AbstractDBRepository;
use JTL\Catalog\Product\Artikel;
use JTL\DB\ReturnType;
use JTL\Helpers\Typifier;
use JTL\RMA\DomainObjects\RMADomainObject;
use JTL\RMA\DomainObjects\RMAItemDomainObject;
use JTL\RMA\DomainObjects\RMAReasonLangDomainObject;
use JTL\RMA\DomainObjects\RMAReturnAddressDomainObject;
use JTL\RMA\Helper\RMAItems;
use ReflectionNamedType;
use stdClass;

/**
 * Class RMARepository
 * @package JTL\RMA\Repositories
 * @since 5.3.0
 * @description This is a layer between the RMA Service and the database.
 */
class RMARepository extends AbstractDBRepository
{
    /**
     * @return string
     */
    public function getTableName(): string
    {
        return 'rma';
    }

    /**
     * @param int   $langID
     * @param array $filter
     * @return stdClass
     * @todo filter could be an object instead of an array. So the developer has just to fill in the desired properties.
     */
    private function buildQuery(int $langID, array $filter = []): stdClass
    {
        $param   = ['langID' => $langID];
        $queries = [
            'customerID'    => '',
            'id'            => '',
            'status'        => '',
            'beforeDate'    => '',
            'returnAddress' => '',
            'product'       => '',
            'shippingNote'  => '',
            'synced'        => ''
        ];

        if (isset($filter['customerID'])) {
            $param['customerID']   = $filter['customerID'];
            $queries['customerID'] = ' AND rma.customerID = :customerID';
        }
        if (isset($filter['id'])) {
            $param['id']   = $filter['id'];
            $queries['id'] = ' AND rma.id = :id';
        }
        if (isset($filter['status'])) {
            $param['status']   = $filter['status'];
            $queries['status'] = ' AND rma.status = :status';
        }
        if (isset($filter['beforeDate'])) {
            $param['beforeDate']   = $filter['beforeDate'];
            $queries['beforeDate'] = ' AND rma.createDate < :beforeDate';
        }
        if (isset($filter['product'])) {
            $param['pID']       = $filter['product'];
            $queries['product'] = ' AND items.productID = :pID';
        }
        if (isset($filter['shippingNote'])) {
            $param['sID']            = $filter['shippingNote'];
            $queries['shippingNote'] = ' AND shippingNote.kLieferschein = :sID';
        }
        if (isset($filter['synced'])) {
            $param['synced']   = $filter['synced'];
            $queries['synced'] = ' AND rma.synced = :synced';
        }

        $result         = new stdClass();
        $result->params = $param;
        $result->stmnt  =
            'SELECT
                rma.id AS rma_id,
                rma.wawiID,
                rma.customerID,
                rma.replacementOrderID,
                rma.rmaNr,
                rma.voucherCredit,
                rma.refundShipping,
                rma.synced AS rma_synced,
                rma.status AS rma_status,
                rma.comment AS rma_comment,
                rma.createDate AS rma_createDate,
                rma.lastModified AS rma_lastModified,
                
                items.id AS rma_item_id,
                items.orderPosID,
                items.orderID,
                items.productID,
                items.reasonID,
                items.name,
                items.variationProductID,
                items.variationName,
                items.variationValue,
                items.partListProductID,
                items.partListProductName,
                items.partListProductURL,
                items.partListProductNo,
                items.unitPriceNet,
                items.quantity,
                items.vat,
                items.unit,
                
                items.shippingNotePosID,
                items.comment AS rma_item_comment,
                items.status AS rma_item_status,
                items.createDate AS rma_item_createDate,
    
                return_address.id AS return_address_id,
                return_address.salutation AS return_address_salutation,
                return_address.firstName AS return_address_firstName,
                return_address.lastName AS return_address_lastName,
                return_address.academicTitle AS return_address_academicTitle,
                return_address.companyName AS return_address_companyName,
                return_address.companyAdditional AS return_address_companyAdditional,
                return_address.street AS return_address_street,
                return_address.houseNumber AS return_address_houseNumber,
                return_address.addressAdditional AS return_address_addressAdditional,
                return_address.postalCode AS return_address_postalCode,
                return_address.city AS return_address_city,
                return_address.state AS return_address_state,
                return_address.countryISO AS return_address_countryISO,
                return_address.phone AS return_address_phone,
                return_address.mobilePhone AS return_address_mobilePhone,
                return_address.fax AS return_address_fax,
                return_address.mail AS return_address_mail,
                
                rma_reasons.id AS rma_reasons_id,
                rma_reasons.wawiID AS rma_reasons_wawiID,
                
                rma_reasons_lang.id AS rma_reasons_lang_id,
                rma_reasons_lang.reasonID AS rma_reasons_lang_reasonID,
                rma_reasons_lang.langID AS rma_reasons_lang_langID,
                rma_reasons_lang.title AS rma_reasons_lang_title,
                
                shippingNote.kLieferschein AS shippingNoteID,

                tbestellung.cBestellNr AS orderNo,
                
                tseo.cSeo AS seo,
                
                tartikel.cArtNr AS productNo,
                tartikel.cTeilbar AS isDivisible
            FROM
                rma
                    
            RIGHT JOIN rma_items AS items
                ON rma.id = items.rmaID' . $queries['product'] . '
            
            LEFT JOIN tlieferscheinpos AS shippingNote
                ON items.shippingNotePosID = shippingNote.kLieferscheinPos' . $queries['shippingNote'] . '
            
            LEFT JOIN rma_reasons
                ON items.reasonID = rma_reasons.id
            
            LEFT JOIN rma_reasons_lang
                ON items.reasonID = rma_reasons_lang.reasonID
                AND rma_reasons_lang.langID = :langID
            
            LEFT JOIN return_address
                ON rma.id = return_address.rmaID
            
            LEFT JOIN tbestellung
                ON items.orderID = tbestellung.kBestellung
            
            LEFT JOIN twarenkorbpos
                ON twarenkorbpos.kBestellpos = items.orderPosID
            
            LEFT JOIN twarenkorbposeigenschaft
                ON twarenkorbposeigenschaft.kWarenkorbPos = twarenkorbpos.kWarenkorbPos
            
            LEFT JOIN teigenschaftsprache
                ON teigenschaftsprache.kEigenschaft = twarenkorbposeigenschaft.kEigenschaft
                AND teigenschaftsprache.kSprache = :langID
            
            LEFT JOIN teigenschaftwertsprache
                ON teigenschaftwertsprache.kEigenschaftWert = twarenkorbposeigenschaft.kEigenschaftWert
                AND teigenschaftwertsprache.kSprache = :langID
            
            LEFT JOIN tseo
                ON tseo.kKey = items.productID
                AND tseo.cKey = "kArtikel"
                AND tseo.kSprache = :langID
                
            LEFT JOIN tartikel
                ON tartikel.kArtikel = items.productID
            
            WHERE rma.id > 0'
            . $queries['customerID']
            . $queries['id']
            . $queries['status']
            . $queries['beforeDate']
            . $queries['returnAddress']
            . $queries['synced']
            . ' GROUP BY items.id'
            . ' ORDER BY rma.id DESC';

        return $result;
    }

    /**
     * @param array $dataFromDB
     * @param array $defaultData
     * @param string $columnPrefix
     * @return array
     */
    private function defaultDomainObjectData(array $dataFromDB, array $defaultData, string $columnPrefix): array
    {
        $result = [];
        foreach ($defaultData as $key => $type) {
            $result[$key] = $this->setFromDefaultValues(
                columnPrefix: $columnPrefix,
                key: $key,
                type: $type,
                data: $dataFromDB
            );
        }

        return $result;
    }

    /**
     * @param int      $langID
     * @param array    $filter
     * @param int|null $limit
     * @return RMADomainObject[]
     * @throws \Exception
     * @since 5.3.0
     */
    public function getReturns(
        int $langID,
        array $filter = [],
        ?int $limit = null
    ): array {
        $rmas = [];

        $readableQuery = $this->buildQuery(
            $langID,
            $filter
        );
        foreach ($this->db->executeQueryPrepared(
            stmt  : $readableQuery->stmnt,
            params: $readableQuery->params,
            return: ReturnType::ARRAY_OF_ASSOC_ARRAYS
        ) as $joinedDBData) {
            if (!isset($rmas[$joinedDBData['rma_id']])) {
                $rmas[$joinedDBData['rma_id']] = $joinedDBData;

                // instantiate RMA Address Domain Object
                $rmas[$joinedDBData['rma_id']]['returnAddress'] = new RMAReturnAddressDomainObject(
                    id: Typifier::intify($joinedDBData['return_address_id']),
                    rmaID: Typifier::intify($joinedDBData['rma_id']),
                    customerID: Typifier::intify($joinedDBData['customerID']),
                    salutation: Typifier::stringify($joinedDBData['return_address_salutation'] ?? null),
                    firstName: Typifier::stringify($joinedDBData['return_address_firstName']),
                    lastName: Typifier::stringify($joinedDBData['return_address_lastName']),
                    academicTitle: Typifier::stringify($joinedDBData['return_address_academicTitle'] ?? null, null),
                    companyName: Typifier::stringify($joinedDBData['return_address_companyName'] ?? null, null),
                    companyAdditional: Typifier::stringify(
                        $joinedDBData['return_address_companyAdditional'] ?? null,
                        null
                    ),
                    street: Typifier::stringify($joinedDBData['return_address_street']),
                    houseNumber: Typifier::stringify($joinedDBData['return_address_houseNumber']),
                    addressAdditional: Typifier::stringify(
                        $joinedDBData['return_address_addressAdditional'] ?? null,
                        null
                    ),
                    postalCode: Typifier::stringify($joinedDBData['return_address_postalCode']),
                    city: Typifier::stringify($joinedDBData['return_address_city']),
                    state: Typifier::stringify($joinedDBData['return_address_state']),
                    countryISO: Typifier::stringify($joinedDBData['return_address_countryISO']),
                    phone: Typifier::stringify($joinedDBData['return_address_phone'] ?? null, null),
                    mobilePhone: Typifier::stringify($joinedDBData['return_address_mobilePhone'] ?? null, null),
                    fax: Typifier::stringify($joinedDBData['return_address_fax'] ?? null, null),
                    mail: Typifier::stringify($joinedDBData['return_address_mail'] ?? null, null)
                );
            }
            // Instantiate RMA Item Domain Object
            $productID = (int)$joinedDBData['productID'];
            if (!empty($joinedDBData['variationProductID'])) {
                $productID = (int)$joinedDBData['variationProductID'];
            }

            $product           = new Artikel();
            $product->kArtikel = $productID;
            $product->cName    = '';
            $product->cSeo     = $joinedDBData['seo'] ?? '';
            $product->holBilder();
            $product->cTeilbar = $joinedDBData['isDivisible'];

            $rmas[$joinedDBData['rma_id']]['items'][] = new RMAItemDomainObject(
                id: Typifier::intify($joinedDBData['rma_item_id']),
                rmaID: Typifier::intify($joinedDBData['rma_id']),
                shippingNotePosID: Typifier::intify($joinedDBData['shippingNotePosID'] ?? null, null),
                orderID: Typifier::intify($joinedDBData['orderID'] ?? null, null),
                orderPosID: Typifier::intify($joinedDBData['orderPosID'] ?? null, null),
                productID: Typifier::intify($joinedDBData['productID'] ?? null, null),
                reasonID: Typifier::intify($joinedDBData['reasonID'] ?? null, null),
                name: Typifier::stringify($joinedDBData['name']),
                variationProductID: Typifier::intify($joinedDBData['variationProductID'] ?? null, null),
                variationName: Typifier::stringify($joinedDBData['variationName'] ?? null, null),
                variationValue: Typifier::stringify($joinedDBData['variationValue'] ?? null, null),
                partListProductID: Typifier::intify($joinedDBData['partListProductID'] ?? null, null),
                partListProductName: Typifier::stringify($joinedDBData['partListProductName'] ?? null, null),
                partListProductURL: Typifier::stringify($joinedDBData['partListProductURL'] ?? null, null),
                partListProductNo: Typifier::stringify($joinedDBData['partListProductNo'] ?? null, null),
                unitPriceNet: Typifier::floatify($joinedDBData['unitPriceNet']),
                quantity: Typifier::floatify($joinedDBData['quantity'] ?? null, 1.00),
                vat: Typifier::floatify($joinedDBData['vat'] ?? null, 0.00),
                unit: Typifier::stringify($joinedDBData['unit'] ?? null, null),
                comment: Typifier::stringify($joinedDBData['rma_item_comment'] ?? null, null),
                status: Typifier::stringify($joinedDBData['rma_item_status'] ?? null, null),
                createDate: Typifier::stringify($joinedDBData['rma_item_createDate'] ?? null, null),
                product: $product,
                reason: new RMAReasonLangDomainObject(
                    id: Typifier::intify($joinedDBData['rma_reasons_lang_id'] ?? null),
                    reasonID: Typifier::intify($joinedDBData['rma_reasons_id'] ?? null),
                    langID: Typifier::intify($langID ?? null),
                    title: Typifier::stringify($joinedDBData['rma_reasons_lang_title'] ?? null),
                ),
                productNo: Typifier::stringify($joinedDBData['productNo'] ?? null, null),
                orderStatus: RMADomainObject::orderStatusToString(
                    Typifier::intify($joinedDBData['rma_status'] ?? null)
                ),
                seo: Typifier::stringify($joinedDBData['seo'] ?? null, null),
                orderNo: Typifier::stringify($joinedDBData['orderNo'] ?? null, null),
                customerID: Typifier::intify($joinedDBData['customerID'] ?? null, null),
                shippingNoteID: Typifier::intify($joinedDBData['shippingNoteID'] ?? null, null)
            );
        }

        $result = [];
        foreach ($rmas as $rma) {
            if ($limit !== null) {
                if ($limit <= 0) {
                    break;
                }
                $limit--;
            }
            $rmaID          = Typifier::intify($rma['rma_id']);
            $result[$rmaID] = new RMADomainObject(
                id: $rmaID,
                wawiID: Typifier::intify($rma['wawiID'] ?? null, null),
                customerID: Typifier::intify($rma['customerID'] ?? null),
                replacementOrderID: Typifier::intify($rma['replacementOrderID'] ?? null, null),
                rmaNr: Typifier::stringify($rma['rmaNr'] ?? null, null),
                voucherCredit: Typifier::boolify($rma['voucherCredit'] ?? null, null),
                refundShipping: Typifier::boolify($rma['refundShipping'] ?? null, null),
                synced: Typifier::boolify($rma['rma_synced']),
                status: Typifier::intify($rma['rma_status']),
                comment: Typifier::stringify($rma['rma_comment'] ?? null, null),
                createDate: Typifier::stringify($rma['rma_createDate'] ?? null, null),
                lastModified: Typifier::stringify($rma['rma_lastModified'] ?? null, null),
                items: new RMAItems($rma['items']),
                returnAddress: $rma['returnAddress']
            );
        }

        return $result;
    }

    /**
     * @param string $columnPrefix
     * @param string $key
     * @param ReflectionNamedType $type
     * @param array $data
     * @return mixed
     */
    private function setFromDefaultValues(
        string $columnPrefix,
        string $key,
        ReflectionNamedType $type,
        array $data
    ): mixed {
        $value = null;
        if (isset($data[$key])) {
            $value = $data[$key];
        } elseif (isset($data[($columnPrefix . $key)])) {
            $value = $data[$columnPrefix . $key];
        }

        if ($value === null) {
            if ($type->allowsNull() !== true) {
                // Set fallback value
                $value = match ($type->getName()) {
                    'string' => '',
                    'int'    => 0,
                    'float'  => 0.00,
                    'array'  => [],
                    'object' => new \stdClass(),
                    'bool'   => false
                };
            }
        } else {
            \settype($value, $type->getName());
        }
        return $value;
    }

    /**
     * @param int $customerID
     * @param int $languageID
     * @param int $cancellationTime
     * @param int $orderID
     * @return RMAItemDomainObject[]
     * @since 5.3.0
     */
    public function getReturnableProducts(
        int $customerID,
        int $languageID,
        int $cancellationTime,
        int $orderID = 0
    ): array {
        $result   = [];
        $products = [];

        foreach ($this->db->getObjects(
            "SELECT twarenkorbpos.kArtikel AS id, twarenkorbpos.cEinheit AS unit,
        twarenkorbpos.cArtNr AS productNo, twarenkorbpos.fPreisEinzelNetto AS unitPriceNet, twarenkorbpos.fMwSt AS vat,
        twarenkorbpos.cName AS name, twarenkorbpos.kBestellpos AS orderPosID, tbestellung.kKunde AS customerID,
        tbestellung.kLieferadresse AS shippingAddressID, tbestellung.cStatus AS orderStatus,
        tbestellung.cBestellNr AS orderNo, tbestellung.kBestellung AS orderID,
        tlieferscheinpos.kLieferscheinPos AS shippingNotePosID, tlieferscheinpos.kLieferschein AS shippingNoteID,
        (tlieferscheinpos.fAnzahl - SUM(IFNULL(rma_items.quantity, 0))) AS quantity, tartikel.cSeo AS seo,
        tartikel.cTeilbar AS isDivisible, DATE_FORMAT(FROM_UNIXTIME(tversand.dErstellt), '%d-%m-%Y') AS createDate,
        twarenkorbposeigenschaft.cEigenschaftName AS propertyName,
        twarenkorbposeigenschaft.cEigenschaftWertName AS propertyValue,
        teigenschaftsprache.cName AS propertyNameLocalized, teigenschaftwertsprache.cName AS propertyValueLocalized,
        GROUP_CONCAT(tstueckliste.kArtikel SEPARATOR ',') AS partListProductIDs
            FROM tbestellung
            INNER JOIN twarenkorbpos
                ON twarenkorbpos.kWarenkorb = tbestellung.kWarenkorb
                AND twarenkorbpos.kArtikel > 0
            INNER JOIN tlieferscheinpos
                ON tlieferscheinpos.kBestellPos = twarenkorbpos.kBestellpos
            INNER JOIN tversand
                ON tversand.kLieferschein = tlieferscheinpos.kLieferschein
                AND DATE(FROM_UNIXTIME(tversand.dErstellt)) >= DATE_SUB(NOW(), INTERVAL :cancellationTime DAY)
            LEFT JOIN tartikelattribut
                ON tartikelattribut.kArtikel = twarenkorbpos.kArtikel
                AND tartikelattribut.cName = :notReturnable
            LEFT JOIN tartikeldownload
                ON tartikeldownload.kArtikel = twarenkorbpos.kArtikel
            LEFT JOIN twarenkorbposeigenschaft
                ON twarenkorbposeigenschaft.kWarenkorbPos = twarenkorbpos.kWarenkorbPos
            LEFT JOIN teigenschaftsprache
                ON teigenschaftsprache.kEigenschaft = twarenkorbposeigenschaft.kEigenschaft
                AND teigenschaftsprache.kSprache = :langID
            LEFT JOIN teigenschaftwertsprache
                ON teigenschaftwertsprache.kEigenschaftWert = twarenkorbposeigenschaft.kEigenschaftWert
                AND teigenschaftwertsprache.kSprache = :langID
            INNER JOIN tartikel
                ON tartikel.kArtikel = twarenkorbpos.kArtikel
            LEFT JOIN tstueckliste
            	ON tartikel.kStueckliste = tstueckliste.kStueckliste
            LEFT JOIN rma_items
                ON rma_items.shippingNotePosID = tlieferscheinpos.kLieferscheinPos
                AND tlieferscheinpos.kBestellPos = twarenkorbpos.kBestellpos
                AND rma_items.productID = twarenkorbpos.kArtikel
            WHERE tbestellung.kKunde = :customerID
                AND tbestellung.cStatus IN (:status_versandt, :status_teilversandt)
                AND tartikelattribut.cWert IS NULL
                AND tartikeldownload.kArtikel IS NULL"
                . ($orderID > 0 ? ' AND tbestellung.kBestellung = :orderID' : '')
                . ' GROUP BY tlieferscheinpos.kLieferscheinPos',
            [
                'customerID'          => $customerID,
                'langID'              => $languageID,
                'orderID'             => $orderID,
                'status_versandt'     => \BESTELLUNG_STATUS_VERSANDT,
                'status_teilversandt' => \BESTELLUNG_STATUS_TEILVERSANDT,
                'cancellationTime'    => $cancellationTime,
                'notReturnable'       => \FKT_ATTRIBUT_PRODUCT_NOT_RETURNABLE
            ]
        ) as $product) {
            if ((float)$product->quantity <= 0) {
                continue;
            }
            $partListProductIDs           = Typifier::stringify($product->partListProductIDs ?? null, null);
            $product->partListProductID   = 0;
            $product->partListProductName = '';
            $product->partListProductNo   = '';
            $product->partListProductURL  = '';

            $product->id                 = Typifier::intify($product->id);
            $product->rmaID              = 0;
            $product->shippingNotePosID  = Typifier::intify($product->shippingNotePosID ?? null, null);
            $product->orderID            = Typifier::intify($product->orderID ?? null, null);
            $product->orderPosID         = Typifier::intify($product->orderPosID ?? null, null);
            $product->productID          = Typifier::intify($product->productID ?? null, null);
            $product->reasonID           = Typifier::intify($product->reasonID ?? null, null);
            $product->variationProductID = Typifier::intify($product->variationProductID ?? null, null);
            $product->orderStatus        = Typifier::stringify($product->orderStatus ?? null, null);
            $product->unitPriceNet       = Typifier::floatify($product->unitPriceNet);
            $product->quantity           = Typifier::floatify($product->quantity ?? null, 1.00);
            $product->vat                = Typifier::floatify($product->vat ?? null, 0.00);
            $product->history            = Typifier::arrify($product->history ?? null, null);
            $product->customerID         = Typifier::intify($product->customerID ?? null, null);
            $product->shippingAddressID  = Typifier::intify($product->shippingAddressID ?? null, null);
            $product->shippingNoteID     = Typifier::intify($product->shippingNoteID ?? null, null);
            $product->name               = Typifier::stringify($product->name ?? null);
            $product->unit               = Typifier::stringify($product->unit ?? null, null);
            $product->productNo          = Typifier::stringify($product->productNo ?? null, null);
            $product->orderNo            = Typifier::stringify($product->orderNo ?? null, null);
            $product->seo                = Typifier::stringify($product->seo ?? null, null);
            $product->isDivisible        = Typifier::stringify($product->isDivisible ?? null, null);
            $product->createDate         = Typifier::stringify($product->createDate ?? null, null);

            if (($partListProductIDs ?? '') !== '') {
                foreach ($this->db->getObjects(
                    'SELECT tartikel.kArtikel AS id, tartikel.cName AS name,
                            tartikel.cSeo AS seo, tartikel.cTeilbar AS isDivisible,
                            tartikel.cArtNr AS productNo, tartikelsprache.cName AS nameLocalized,
                            tartikelsprache.cSeo AS seoLocalized, rma_items.quantity AS returnedQuantity,
                            tstueckliste.fAnzahl AS partListQuantity
                            FROM tartikel
                            LEFT JOIN tartikelsprache
                                ON tartikelsprache.kArtikel = tartikel.kArtikel
                                AND tartikelsprache.kSprache = :langID
                            LEFT JOIN rma_items
                                ON rma_items.productID = tartikel.kArtikel
                                AND rma_items.shippingNotePosID = :shippingNotePosID
                            LEFT JOIN tstueckliste
                                ON tstueckliste.kArtikel = tartikel.kArtikel
                            WHERE tartikel.kArtikel IN (' . $partListProductIDs . ')',
                    [
                        'langID' => $languageID,
                        'shippingNotePosID' => $product->shippingNotePosID
                    ]
                ) as $partListProduct) {
                    if ($partListProduct->returnedQuantity === null) {
                        $partListProduct->returnedQuantity = 0;
                    }
                    $returnableQuantity = ((float)$partListProduct->partListQuantity * $product->quantity)
                        - (float)$partListProduct->returnedQuantity;
                    if ($returnableQuantity <= 0) {
                        continue;
                    }
                    $compoundData                      = clone($product);
                    $compoundData->partListProductID   = Typifier::intify($product->id);
                    $compoundData->partListProductName = $product->name;
                    $compoundData->partListProductNo   = $product->productNo;
                    $compoundData->partListProductURL  = $product->seo;

                    $compoundData->id          = Typifier::intify($partListProduct->id);
                    $compoundData->name        = Typifier::stringify($partListProduct->nameLocalized ?? null, null)
                        ?? Typifier::stringify($partListProduct->name);
                    $compoundData->quantity    = $returnableQuantity;
                    $compoundData->productNo   = $partListProduct->productNo;
                    $compoundData->isDivisible = $partListProduct->isDivisible;
                    $compoundData->seo         = $partListProduct->seoLocalized ?? $partListProduct->seo;

                    $products[] = $compoundData;
                }
            } else {
                $products[] = $product;
            }
        }

        foreach ($products as $product) {
            $product->variationName  = $product->propertyNameLocalized ?? $product->propertyName ?? null;
            $product->variationValue = $product->propertyValueLocalized ?? $product->propertyValue ?? null;

            $product->product           = new Artikel();
            $product->product->kArtikel = (int)$product->id;
            $product->product->cName    = '';
            $product->product->cSeo     = $product->seo ?? '';
            $product->product->holBilder();
            $product->product->cTeilbar = $product->isDivisible;

            unset(
                $product->isDivisible,
                $product->propertyName,
                $product->propertyValue,
                $product->propertyNameLocalized,
                $product->propertyValueLocalized,
                $product->partListProductIDs
            );

            $result[] = new RMAItemDomainObject(
                id: $product->id,
                rmaID: 0,
                shippingNotePosID: $product->shippingNotePosID,
                orderID: $product->orderID,
                orderPosID: $product->orderPosID,
                productID: $product->productID,
                reasonID: null,
                name: $product->name,
                variationProductID: $product->variationProductID,
                variationName: $product->variationName,
                variationValue: $product->variationValue,
                partListProductID: $product->partListProductID,
                partListProductName: $product->partListProductName,
                partListProductURL: $product->partListProductURL,
                partListProductNo: $product->partListProductNo,
                unitPriceNet: $product->unitPriceNet,
                quantity: $product->quantity,
                vat: $product->vat,
                unit: $product->unit,
                comment: null,
                status: null,
                createDate: null,
                history: null,
                product: $product->product,
                reason: null,
                productNo: $product->productNo,
                orderStatus: $product->orderStatus,
                seo: $product->seo,
                orderNo: $product->orderNo,
                customerID: $product->customerID,
                shippingAddressID: $product->shippingAddressID,
                shippingNoteID: $product->shippingNoteID
            );
        }

        return $result;
    }

    /**
     * @param array $orderIDs
     * @return array
     * @since 5.3.0
     */
    public function orderIDsToNOs(array $orderIDs): array
    {
        $result = [];
        $this->db->getCollection(
            'SELECT tbestellung.kBestellung AS orderID, tbestellung.cBestellNr AS orderNo
            FROM tbestellung
            WHERE tbestellung.kBestellung IN (' . \implode(',', $orderIDs) . ')'
        )->each(function ($obj) use (&$result) {
            $result[(int)$obj->orderID] = $obj->orderNo;
        });

        return $result;
    }

    /**
     * @param RMADomainObject[] $rmaDomainObjects
     * @return array
     * @description Mark returns as synced and return the IDs of returns that could not be marked.
     */
    public function markedAsSynced(array $rmaDomainObjects): array
    {
        if (\count($rmaDomainObjects) === 0) {
            return [];
        }

        $ids = [];
        foreach ($rmaDomainObjects as $rma) {
            $ids[] = $rma->id;
        }

        return $this->db->executeQueryPrepared(
            'UPDATE ' . $this->getTableName() . ' SET synced = 0 WHERE ' . $this->getKeyName()
            . ' IN (' . \implode(',', $ids) . ')',
            [],
            ReturnType::AFFECTED_ROWS
        ) > 0 ? [] : $ids;
    }
}
