<?php declare(strict_types=1);

namespace JTL\Router\Controller;

use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Laminas\Diactoros\Response\RedirectResponse;
use League\Route\RouteGroup;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use JTL\Router\Middleware\FaviconFileCheckMiddleware;

/**
 * Class FaviconController
 * @package JTL\Router\Controller
 */
class FaviconController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function register(RouteGroup $route, string $dynName): void
    {
        $faviconFileCheckMiddleware = new FaviconFileCheckMiddleware();
        $supportedFiles             = '{file:favicon.ico|favicon.svg|apple-touch-icon.png|android-chrome-192x192.png|' .
            'android-chrome-512x512.png|site.webmanifest|browserconfig.xml|safari-pinned-tab.svg|mstile-70x70.png|' .
            'mstile-144x144.png|mstile-150x150.png|mstile-310x150.png|mstile-310x310.png}';
        $route->get(\sprintf('%s', $supportedFiles), [$this, 'getResponse'])
            ->middleware($faviconFileCheckMiddleware);
    }

    /**
     * @inheritdoc
     */
    public function getResponse(ServerRequestInterface $request, array $args, JTLSmarty $smarty): ResponseInterface
    {
        $file         = $args['file'];
        $this->smarty = $smarty;
        $favURL       = $this->getFaviconURL(Shop::getURL(), $file);
        if (!$this->init()) {
            return $this->notFoundResponse($request, $args, $smarty);
        }

        return new RedirectResponse($favURL, 301);
    }
}
