<?php declare(strict_types=1);

use JTL\Update\IMigration;
use JTL\Update\Migration;

/**
 * Class Migration_20231016122200
 */
class Migration_20231016122200 extends Migration implements IMigration
{
    protected $author = 'fm';
    protected $description = 'Remove APC caching method';

    /**
     * @inheritdoc
     */
    public function up(): void
    {
        $this->execute('DELETE FROM teinstellungenconfwerte
            WHERE kEinstellungenConf = 1551
                AND cName = \'APC\''
        );
    }

    /**
     * @inheritdoc
     */
    public function down(): void
    {
        $this->execute("
            INSERT INTO teinstellungenconfwerte 
            (`kEinstellungenConf`, `cName`, `cWert`, `nSort`)
            VALUES ('1551','APC','apc','3')"
        );
    }
}
